function filterBy(array, type) {
    return array.reduce((currentType, currentItem) => { if ( typeof currentItem != type) {  currentType.push(currentItem);} return currentType} , []);
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'))